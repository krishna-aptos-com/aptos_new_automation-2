import {RestUtils} from '../../utils/restUtils';
import { URL } from "url"
require('../../hooks/restapihooks.ts');
const {When, Then} = require('cucumber');
const RestUtilsobj: RestUtils = new RestUtils();
let response;

When(/^send GET request to url "([^"]*)"$/,async (url: string) => {
   const headerOptions: string = JSON.stringify({ 'Authorization': RestUtils.getAccessToken()});
   console.log("JSONSTRING ==================== "+ headerOptions);
    response = await RestUtilsobj.makeHttpRequest(url, JSON.parse(headerOptions) );
    console.log(url + '\n');
    console.log(response.body);
});

Then(/^user response includes the following$/, async (expectedresponse) => {
    RestUtilsobj.validateResponse(await RestUtilsobj.responseData, expectedresponse);

});

Then(/^response includes the following in any order$/, async ( responseFields ) => {
    RestUtilsobj.validateJsonPathWithRegularExpression(await RestUtilsobj.responseData, responseFields);

});

Then(/^response not includes the following in any order$/, async ( responseFields ) => {
    RestUtilsobj.invalidJsonPathWithRegularExpression(await RestUtilsobj.responseData, responseFields);

});

When(/^POST a url "([^"]*)" request with input body is$/, async (url: string, inputbody: string) => {

    console.log( inputbody);
    let str = JSON.stringify(inputbody);
    console.log('JSON KR ' + str);
    let randomUserName:string = 'randomuser1240';
    const headerOptions: string = JSON.stringify({ 'Authorization': RestUtils.getAccessToken()});
   // stringified.replace(/"eyes":"blue"/gm, '"eyes":"blue"')
    let requestbody = inputbody.replace('kris123418',  randomUserName);
    console.log("JSONSTRING ==================== "+ headerOptions);
    console.log(url + '\n');
    response = await RestUtilsobj.makeHttpRequest(url,  JSON.parse(headerOptions), 'POST' , requestbody);
    console.log(response);
    const json = JSON.parse(response.body);
    console.log('USER ID ==='+ typeof (json.id) + '    '+ json.id);
    RestUtils.setStatusCode(response.statusCode);
    RestUtils.setUserId(json.id);
    console.log("GET USER ID"+ RestUtils.getUserId());
});

When(/^send a url "([^"]*)" request to DELETE user$/, async (url: string) =>{

    const headerOptions: string = JSON.stringify({ 'Authorization': RestUtils.getAccessToken()});
    console.log("JSONSTRING ==================== "+ headerOptions);
    response = await RestUtilsobj.makeHttpRequest(url,  JSON.parse(headerOptions), 'DELETE');
    console.log(url + '\n');
    console.log(response);
    RestUtils.setStatusCode(response.statusCode);
});


When(/^send a GET request to url "([^"]*)" request with dynamic userid$/, async (url: string)=>{

    const headerOptions: string = JSON.stringify({ 'Authorization': RestUtils.getAccessToken()});
    const newurl = new URL(url);
    newurl.pathname = newurl.pathname.replace("userid", RestUtils.getUserId());
    console.log("URL============"+newurl.toString() +'-----'+ newurl.pathname);
    response = await RestUtilsobj.makeHttpRequest(newurl.toString(),JSON.parse(headerOptions) );
    console.log(url + '\n');
    console.log(response.body);

});
