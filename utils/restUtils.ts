
require('url-search-params-polyfill');
const got = require('got');
const assert = require('assert');
const {JSONPath} = require('jsonpath-plus');

export class RestUtils {

    public static _statuscode: string;
    public static _userid: string;
    public static setStatusCode(statcode: string): void { this._statuscode = statcode;  }
    public static getStatusCode(): string {  return this._statuscode; }
    public static getAccessToken(): string { return  this._accesstoken; }
    public static setAccessToken(token): void { this._accesstoken = token; }

    public static getUserId(): string { return  this._userid; }
    public static setUserId(userid: string): void { this._userid = userid; }
    private static _accesstoken: any;
    public  _results: any ;


    public get responseData(): any { return this._results;
    }

/* generic method for CRUD operations GET, PUT, POST, PATCH and DELETE*/
    public async makeHttpRequest(url:string,  headerOptions?: Object,
                                 method?: string, inputBody?: string, formFlag?: boolean ){
        console.log("=============================================================================================")
        console.log('URL : '+url+'\nREQUEST : '+'\n HEADERS : ');

        if (headerOptions == null ){
            headerOptions = { Accept: 'application/json'};
        }
        if(inputBody == null){
            inputBody = '';
        } else if (formFlag) {

            inputBody = JSON.parse(inputBody);
        }
        if(method == null){
            method = 'GET'; //default GET method
        }

        if(formFlag == null){
            formFlag = false;
        }
        this._results = await got(url,{
            headers : headerOptions,
            body: inputBody,
            form: formFlag,
            method: method
        });


        return this._results;
    }

    //validating response without giving json path
    public validateResponse(response, responseFields) {
        const responsemap = responseFields.rowsHash();
        const userStr = JSON.stringify(responsemap);
        console.log('Expected response as json string' + userStr);
        let expectedvalue: boolean = false;
        JSON.parse(userStr, (key1, value1) => {
            if (value1 instanceof  Object === false) {
                expectedvalue = false;
                JSON.parse(response.body, (key2, value2) => {
                    if (key1 == key2 && value1 == value2) {
                        expectedvalue = true;
                        assert.strictEqual(value1.toString(), value2.toString(), key1  + ' or ' + value1  +
                            ' is not present in response');
                    }
                });
                if ( expectedvalue === false ) {
                    assert.strictEqual(false, true, key1  + ' or ' + value1  +
                        ' is not present in response');
                }
            }
        });

    }

    /*Validation for json response after making http request by giving json path */
    public validateJsonPathWithRegularExpression(response, responseFields) {
        const responsemap = responseFields.rowsHash();
        console.log(responseFields);
        console.log(responsemap);
        const jsons = JSON.parse(response.body);
        const userStr = JSON.stringify(responsemap);
        console.log('response fields from feature file' + userStr);
        JSON.parse(userStr, (key1, value1) => {
            console.log('JSON PATH KEY! ' + key1);
            if (value1 instanceof Object === false) {
                const result = JSONPath({path: key1, json: jsons});
                console.log('JSON RESULT ' + result);
                if ( value1.includes(',')) {
                    console.log('VALUE! ' + value1);
                    const str = value1.toString().split(',');
                    str.forEach((value) => {

                        console.log('SPLITTED ' + value);
                        console.log('RESULTSS ' + result);
                        if (result.includes(value)) {
                            assert.strictEqual(true, true, key1 + ' or ' + value +
                                ' is not present in response');
                        } else {

                            assert.deepStrictEqual(false, true,
                                key1 + ' or ' + value + ' is not present in response');
                        }
                    });
                } else if (result.includes(value1)) {
                    assert.strictEqual(true, true, key1 + ' or ' + value1 +
                        ' is not present in response');
                } else {

                    assert.deepStrictEqual(false, true,
                        key1 + ' or ' + value1 + ' is not present in response');
                }
            }
        });
    }

    /*Validation for negative scenarios */
    public invalidJsonPathWithRegularExpression(response, responseFields) {
        const responsemap = responseFields.rowsHash();
        console.log(responseFields);
        console.log(responsemap);
        const jsons = JSON.parse(response.body);
        const userStr = JSON.stringify(responsemap);
        console.log('response fields from feature file' + userStr);
        JSON.parse(userStr, (key1, value1) => {
            console.log('JSON PATH KEY! ' + key1);
            if (value1 instanceof Object === false) {
                const result = JSONPath({path: key1, json: jsons});
                console.log('JSON RESULT ' + result);
                if ( value1.includes(',')) {
                    console.log('VALUE! ' + value1);
                    const str = value1.toString().split(',');
                    str.forEach((value) => {

                        console.log('SPLITTED ' + value);
                        console.log('RESULTSS ' + result);
                        if (result.includes(value)) {
                            assert.strictEqual(true, false, key1 + ' or ' + value +
                                ' is  present in response');
                        } else {

                            assert.deepStrictEqual(true, true,
                                key1 + ' or ' + value + ' is not present in response');
                        }
                    });
                } else if (result.includes(value1)) {
                    assert.strictEqual(true, false, key1 + ' or ' + value1 +
                        ' is  present in response');
                } else {

                    assert.deepStrictEqual(true, true,
                        key1 + ' or ' + value1 + ' is not present in response');
                }
            }
        });
    }


}
