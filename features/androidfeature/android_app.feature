Feature: Aptos Store Selling App

Scenario Outline: Invalid credentials scenario
Given User opens Aptos Store Selling Android App and provided the Test Environment for Platform Domain as <platformDomain> and clicks on Done button
When User provides invalid credentials with <username> and <password> and clicks on Log in button
Then User should not be logged and should prompt an invalid username and password error message
Examples:

             | platformDomain                 | username | password     |
             | test.aptos-denim.aptos-labs.io | username  | password|      

             # Add  invalid credentials


Scenario Outline: Valid credentials scenario
Given User opens Aptos Store Selling Android App and provided the Test Environment for Platform Domain as <platformDomain> and clicks on Done button
When User provides valid credentials with <username> and <password> and clicks on Log in button
Then should be able login successfully

Examples:

               | platformDomain                 | username | password |
               | test.aptos-denim.aptos-labs.io | valid username  | valid password |

            # Add your valid credentials

Scenario: Validate Store Associate Settings page
Given User Logged in successfully and verify the Setting page
When User select Chicago retail Store and provides the Terminal Number as 145_0001 and click on next transaction Number
Then Validate Store Associate Login page
